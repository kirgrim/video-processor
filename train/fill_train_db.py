import pandas as pd
import sqlite3
import os
from sqlite3 import Error
from PIL import Image
import urllib.request

PHOTOS_SOURCE_FILE = 'unsplash_dataset/photos.txt'

KEYWORDS_SOURCE_FILE = 'unsplash_dataset/keywords.txt'

DATABASE_URL = r"{current_dir}/train_db.db".format(current_dir=os.getcwd())

SQL_DROP_TABLE_KEYWORD = "DROP TABLE keywords"

SQL_DROP_TABLE_PHOTOS = "DROP TABLE photos"

SQL_CREATE_TABLE_KEYWORD = """ CREATE TABLE IF NOT EXISTS keywords (
                                        keyword_id integer PRIMARY KEY AUTOINCREMENT,
                                        photo_id text NOT NULL,
                                        keyword text NOT NULL,
                                         FOREIGN KEY ( photo_id ) REFERENCES photos(photo_id)
                                    ); """

SQL_CREATE_TABLE_PHOTOS = """CREATE TABLE IF NOT EXISTS photos (
                                    photo_id text PRIMARY KEY,
                                    photo_image_url text NOT NULL,
                                    photo_width text NOT NULL,
                                    photo_height text NOT NULL
                                ); """


def create_db_connection(db_file):
    connection = None
    try:
        connection = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return connection


def create_table(conn, create_table_sql):
    if "CREATE TABLE" not in create_table_sql:
        raise Exception('Invalid create query provided')
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def drop_table(conn, drop_table_sql):
    if "DROP TABLE" not in drop_table_sql:
        raise Exception('Invalid drop query provided')
    try:
        c = conn.cursor()
        c.execute(drop_table_sql)
    except Error as e:
        print(e)


def add_photos(conn, photo_lines):
    c = conn.cursor()
    print(photo_lines[1])
    for photo_line in photo_lines:
        c.execute('BEGIN TRANSACTION')
        c.execute('INSERT OR IGNORE INTO photos (photo_id, photo_image_url, photo_width, photo_height) VALUES (?,?,?,?)'
                  , (photo_line[0], photo_line[2], photo_line[6], photo_line[7]))
        c.execute('COMMIT')


def add_keywords(conn, keyword_lines):
    c = conn.cursor()
    print(keyword_lines[0])
    for keyword_line in keyword_lines:
        c.execute('BEGIN TRANSACTION')
        c.execute('INSERT OR IGNORE INTO keywords (photo_id, keyword) VALUES (?,?)', (keyword_line[0], keyword_line[1]))
        c.execute('COMMIT')


def generate_db_and_fill_with_data(connection):
    drop_table(conn=connection, drop_table_sql=SQL_DROP_TABLE_KEYWORD)
    drop_table(conn=connection, drop_table_sql=SQL_DROP_TABLE_PHOTOS)

    create_table(conn=connection, create_table_sql=SQL_CREATE_TABLE_PHOTOS)
    create_table(conn=connection, create_table_sql=SQL_CREATE_TABLE_KEYWORD)

    with open(PHOTOS_SOURCE_FILE, 'r', encoding='utf-8') as f:
        lines = f.readlines()
        lines.pop(0)
        add_photos(conn=connection, photo_lines=[_.split() for _ in lines])

    with open(KEYWORDS_SOURCE_FILE, 'r', encoding='utf-8') as f:
        lines = f.readlines()
        lines.pop(0)
        add_keywords(conn=connection, keyword_lines=[_.split() for _ in lines])


if __name__ == '__main__':
    conn = create_db_connection(DATABASE_URL)
    if conn:
        pd.set_option('max_colwidth', 800)

        SQL_Query = pd.read_sql_query(
            '''select
            photos.photo_id,photo_image_url,keyword
            from keywords,photos where photos.photo_id = keywords.photo_id and keyword in ("plant") ''', conn)

        SQL_Query_Count = pd.read_sql_query(
            '''select 
            keyword,count(keyword) as num_keyword
            from keywords group by keyword having num_keyword>1000''', conn)

        print(SQL_Query_Count)

        print(SQL_Query['photo_image_url'])
        urllib.request.urlretrieve(SQL_Query['photo_image_url'][0], "local-filename.jpg")

        # img = im = Image.open(response.raw)

        conn.close()
