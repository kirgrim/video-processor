from math import sqrt
import json
import logging
import numpy as np
from mean_average_precision import MeanAveragePrecision2d

def get_classes_from_file(file_path=""):
    classes = []
    with open(file_path, "r") as f:
        for line in f.readlines():
            classes.append(line[:-1])
    return classes


def euclidean_shift(previous_bboxes, current_bboxes):
    """ Calculates euclidean shift between previous and current state of trackable object """
    previous_center = ((previous_bboxes[2] - previous_bboxes[0]) / 2, (previous_bboxes[3] - previous_bboxes[1]) / 2)
    current_center = ((current_bboxes[2] - current_bboxes[0]) / 2, (current_bboxes[3] - current_bboxes[1]) / 2)
    return sqrt(pow((previous_center[0] - current_center[0]), 2) + pow((previous_center[1] - current_center[1]), 2))

def iou(previous_bboxes, current_bboxes):
    """Calculates IOU for two rectangle boxes"""
    
    max_x1 = max(previous_bboxes[0], current_bboxes[0])
    max_y1 = max(previous_bboxes[1], current_bboxes[1])
    max_x2 = min(previous_bboxes[2], current_bboxes[2])
    max_y2 = min(previous_bboxes[3], current_bboxes[3])

    intersection = max(0, max_x2 - max_x1 + 1) * max(0, max_y2 - max_y1 + 1)

    previous_bboxes_area = (previous_bboxes[2] - previous_bboxes[0] + 1) * (previous_bboxes[3] - previous_bboxes[1] + 1)
    current_bboxes_area = (current_bboxes[2] - current_bboxes[0] + 1) * (current_bboxes[3] - current_bboxes[1] + 1)
    
    union = float(previous_bboxes_area + current_bboxes_area - intersection)
    
    if union > 0:
    
        iou = intersection / union

        return iou
    
    return 1



def estimate_precision(bounding_boxes_dict, kwargs):
    """Estimates mAP for bounding box predictions
       kwargs require source_file and frames_limit
       returns:mAP score
    """
    mean_ap_score = 0

    scores = []
    
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    with open(kwargs['source_file'], "r") as source:

        source_data = json.load(source)

        if len(bounding_boxes_dict.keys()) > 0:

            for frame in range(kwargs['frames_limit']):

                if frame in bounding_boxes_dict.keys():

                    if len(bounding_boxes_dict[frame]) > 0:

                        for idx in range(len(source_data['bounding_boxes'][str(frame)])):

                            for j in range(len(source_data['bounding_boxes'][str(frame)][idx])):
                                source_data['bounding_boxes'][str(frame)][idx][j] = int(
                                    source_data['bounding_boxes'][str(frame)][idx][j])

                                source_data['bounding_boxes'][str(frame)][idx][4] = 0

                        for idx in range(len(bounding_boxes_dict[frame])):
                            bounding_boxes_dict[frame][idx][4] = 0

                        logging.debug(f"Ground truth: {source_data['bounding_boxes'][str(frame)]}")

                        logging.debug(f"Predictions: {bounding_boxes_dict[frame]}")

                        ground_truth_bounding_boxes = np.array(source_data['bounding_boxes'][str(frame)])

                        predictions_bounding_boxes = np.array(bounding_boxes_dict[frame])

                        metric_fn = MeanAveragePrecision2d(num_classes=1)

                        for i in range(10):
                            metric_fn.add(predictions_bounding_boxes, ground_truth_bounding_boxes)

                        ap_score = metric_fn.value(iou_thresholds=np.arange(0.5, 1.0, 0.05),
                                                   recall_thresholds=np.arange(0., 1.01, 0.01), mpolicy='soft')['mAP']

                        logging.debug(f"COCO mAP: {ap_score}")

                        scores.append(ap_score)

                else:

                    scores.append(0)

    if len(scores) > 0:
        logging.debug(f"Scores: {scores}")

        logging.debug(f"Scores length: {len(scores)}")

        mean_ap_score = sum(scores) / kwargs['frames_limit']

    return mean_ap_score


from csv import writer

def write_csv(file, result):
        
    with open(file,'a+') as write_obj:

        csv_writer = writer(write_obj)

        csv_writer.writerow(result)
