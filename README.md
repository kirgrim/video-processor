# Video Processor

This project is aimed on buiding a Computer Vision tool, which will provide user with friendly interface for training and evaluating detection models.
Developed with the help of:
- [Anaconda](https://anaconda.org/)
- [Jupyter Notebook](https://jupyter.org/)
- [Open CV](https://opencv.org/)
- [YOLO](https://pjreddie.com/media/files/papers/YOLOv3.pdf)
- [Darknet(Adoptation by AlexeyAB)](https://github.com/AlexeyAB/darknet)
- [PyQt](https://pypi.org/project/PyQt5/)
